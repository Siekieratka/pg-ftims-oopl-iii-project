##Rafał Sojecki
#FilterNOTPro 0.1
This application allows you to make quick calculation of passive filters. Except that, it can chose elements instead of 
you. You need to pass frequency of filter, chose type (RC/RL) and scope of elements with you got in cabinet
(series and multipliers). As output you get parameter of filter elements. Filter is created with the lowest resistance 
possible with accuracy of 5%. Application uses Swing library.
##Ho to run it?
Compiled program is in out\artifacts\pg_ftims_oopl_iii_project_jar catalog. Double click to run.


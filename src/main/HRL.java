package main;

/**
 * Created by rafal on 18.01.2016.
 */
public class HRL extends RL implements CompleteFilter{
    public HRL(FilterPart firstElement, FilterPart secondElement) {
        super(firstElement, secondElement);
    }

    public String getImageUrl(){
        return "LRHiPass.png";
    }
}

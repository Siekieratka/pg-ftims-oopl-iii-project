package main;

/**
 * Created by rafal on 16.01.2016.
 */
public class Capacitor extends Element implements FilterPart {
    public Capacitor(double value) {
        super(value);
    }

    @Override
    public char getType() {
        return 'c';
    }
}

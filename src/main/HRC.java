package main;

/**
 * Created by rafal on 18.01.2016.
 */
public class HRC extends RC implements CompleteFilter {
    public HRC(FilterPart firstElement, FilterPart secondElement) {
        super(firstElement, secondElement);
    }

    public String getImageUrl() {
        return "CRHiPass.png";
    }
}
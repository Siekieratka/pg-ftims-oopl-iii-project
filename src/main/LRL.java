package main;

/**
 * Created by rafal on 18.01.2016.
 */
public class LRL extends RL implements CompleteFilter{
    public LRL(FilterPart firstElement, FilterPart secondElement) {
        super(firstElement, secondElement);
    }

    public String getImageUrl(){
        return "LRLowPass.png";
    }
}

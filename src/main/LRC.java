package main;

/**
 * Created by rafal on 18.01.2016.
 */
public class LRC extends RC implements CompleteFilter {
    public LRC(FilterPart firstElement, FilterPart secondElement) {
        super(firstElement, secondElement);
    }

    public String getImageUrl(){
        return "CRLowPass.png";
    }
}

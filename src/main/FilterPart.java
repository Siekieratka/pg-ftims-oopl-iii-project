package main;

/**
 * Created by rafal on 16.01.2016.
 */
public interface FilterPart {
    double getValue();
    char getType();
}

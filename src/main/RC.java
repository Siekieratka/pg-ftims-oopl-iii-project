package main;

/**
 * Created by rafal on 18.01.2016.
 */
public class RC extends Filter {
    public RC(FilterPart firstElement, FilterPart secondElement) {
        super(firstElement,secondElement);
        if (firstElement.getType() != 'r' || secondElement.getType() != 'r' && firstElement.getType() != 'c' || secondElement.getType() != 'c'){
            this.firstElement = firstElement;
            this.secondElement = secondElement;
            this.value = equation();
        }else {

        }
    }
    protected double equation(){

        return 1/(2*Math.PI * firstElement.getValue()*secondElement.getValue());
    }
    public double getElementsValues(){
        return 0.0;
    };
}

package main;

/**
 * Created by rafal on 16.01.2016.
 */
public class Inductor extends Element implements FilterPart {
    public Inductor(double value) {
        super(value);
    }

    @Override
    public char getType() {
        return 'i';
    }
}

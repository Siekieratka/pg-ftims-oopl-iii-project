package main;

/**
 * Created by rafal on 16.01.2016.
 */
public class Resistor extends Element implements FilterPart {
    public Resistor(double value) {
        super(value);
    }

    @Override
    public char getType() {
        return 'r';
    }
}

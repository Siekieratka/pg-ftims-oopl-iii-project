package main;

/**
 * Created by rafal on 18.01.2016.
 */
public abstract class RL extends Filter{
    public RL(FilterPart firstElement, FilterPart secondElement) {
        super(firstElement,secondElement);
        if (firstElement.getType() != 'r' || secondElement.getType() != 'r' && firstElement.getType() != 'i' || secondElement.getType() != 'i'){
            this.firstElement = firstElement;
            this.secondElement = secondElement;
            this.value = equation();
        }else {

        }
    }
    protected double equation(){
        if (firstElement.getType() == 'r'){
            return firstElement.getValue()/(2*Math.PI * secondElement.getValue());
        }
        return secondElement.getValue()/(2*Math.PI * firstElement.getValue());
    }
    public double getElementsValues(){
        return 0.0;
    };
}

package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Stack;

/**
 * Created by rafal on 16.01.2016.
 */
public abstract class Element {
    public Element(double value) {
        if (value > 10000000){
            //throw exeption
        }else{
            this.value = value;
        }
    }
    public Element() {
        this.value = 0;
    }

    public double getValue() {
        return value;
    }

    protected double value;


}

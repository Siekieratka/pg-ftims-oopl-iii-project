package main;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by rafal on 17.01.2016.
 */
public class MainForm extends JFrame{
    private JPanel panel;
    private JRadioButton lowPassRCRadioButton;
    private JRadioButton lowPassRLRadioButton;
    private JRadioButton highPassRCRadioButton;
    private JRadioButton highPassRLRadioButton;
    private JLabel pictureLabel;
    private JButton calculatebutton;
    private JComboBox comboBox1;
    private JCheckBox a10OhmcheckBox;
    private JCheckBox a100OhmcheckBox;
    private JCheckBox a1kOhmcheckBox;
    private JCheckBox a10kOhmcheckBox;
    private JCheckBox a100kOhmcheckBox;
    private JCheckBox a1pcheckBox;
    private JCheckBox a10ncheckBox;
    private JCheckBox a1mcheckBox;
    private JCheckBox a1ncheckBox;
    private JCheckBox a100ucheckBox;
    private JPanel resistorMultiplersPanel;
    private JCheckBox a10pcheckBox;
    private JCheckBox a100pcheckBox;
    private JCheckBox a100ncheckBox;
    private JCheckBox a1ucheckBox;
    private JCheckBox a10ucheckBox;
    private JComboBox comboBox2;
    private JLabel calculatedLabel;
    private JFormattedTextField formattedTextField1;

    private int seriesE3[]= {10, 22, 47};
    private int seriesE6[] = {10, 15, 22, 47, 68};
    private int seriesE12[] = {10, 12, 15, 18, 22, 27, 33, 39, 47, 56, 68, 82};
    private int seriesE24[] = {10, 11, 12, 13, 15, 16, 18, 20, 22, 24, 27, 30, 33, 36, 39, 43, 47, 51, 56, 62, 68, 75, 82, 91};
    private  int[] getSeries(String type) {
        System.out.println(type);
        if (type == "E3"){
            return seriesE3;
        }else if (type == "E6"){
            return seriesE6;
        }else if (type == "E12"){
            return seriesE12;
        }else{
            return seriesE24;
        }
    }

    private  List<JCheckBox> capacitorMultiplers = new ArrayList();

    private List<JCheckBox> resistorMultiplers = new ArrayList();

    private ButtonGroup filterType;

    private void initialize(){
        comboBox1.addItem("E3");
        comboBox1.addItem("E6");
        comboBox1.addItem("E12");
        comboBox1.addItem("E24");

        comboBox2.addItem("E3");
        comboBox2.addItem("E6");
        comboBox2.addItem("E12");
        comboBox2.addItem("E24");

        resistorMultiplers.add(a10OhmcheckBox);
        resistorMultiplers.add(a100OhmcheckBox);
        resistorMultiplers.add(a1kOhmcheckBox);
        resistorMultiplers.add(a10kOhmcheckBox);
        resistorMultiplers.add(a100kOhmcheckBox);

        capacitorMultiplers.add(a1pcheckBox);
        capacitorMultiplers.add(a10ncheckBox);
        capacitorMultiplers.add(a1mcheckBox);
        capacitorMultiplers.add(a1ncheckBox);
        capacitorMultiplers.add(a100ucheckBox);
        capacitorMultiplers.add(a10pcheckBox);
        capacitorMultiplers.add(a100pcheckBox);
        capacitorMultiplers.add(a100ncheckBox);
        capacitorMultiplers.add(a1ucheckBox);
        capacitorMultiplers.add(a10ucheckBox);

        ButtonGroup a = new ButtonGroup();
        filterType = a;
        filterType.add(highPassRCRadioButton);
        filterType.add(highPassRLRadioButton);
        filterType.add(lowPassRCRadioButton);
        filterType.add(lowPassRLRadioButton);

        setContentPane(panel);
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.setResizable(false);
        setVisible(true);
    }

    public MainForm(){
        super("FilterNOTPro 0.1");
        initialize();
        calculatebutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String temp = formattedTextField1.getText();
                Double frequency;
                try {
                    frequency = Double.parseDouble(temp);
                    if (frequency < 0) {
                        throw new BadInputException();
                    }
                    calculateFilter(frequency);
                } catch (NumberFormatException | BadInputException exception) {
                    JOptionPane.showMessageDialog(new JFrame("Error"),"Error - bad frequency input");
                }
            }
        });
    }
    private String convertToStandardNotation(double input){
        if (input > 1) {
            String value = String.valueOf(input);
            Stack stack = new Stack();
            for (int i = 0; i < value.length() - 2; i++) {
                stack.add(value.charAt(i));
            }

            int zeroCounter = 0;
            char tableOfPrefixes[] = {' ', 'k', 'M'};

            while (true) {
                Object a = stack.peek();
                Object b = '0';
                if (a == b) {
                    stack.pop();
                    zeroCounter++;
                } else {
                    break;
                }
            }
            if (zeroCounter % 3 == 0) {
                stack.push(tableOfPrefixes[zeroCounter / 3]);
            } else if (zeroCounter % 3 == 1) {
                stack.push("0");
                stack.push(tableOfPrefixes[(zeroCounter / 3)]);
            } else if (zeroCounter % 3 == 2 && stack.size() == 1) {
                stack.push("0");
                stack.push("0");
                stack.push(tableOfPrefixes[(zeroCounter / 3)]);
            } else {
                Object temp = stack.peek();
                stack.pop();
                stack.push(tableOfPrefixes[(zeroCounter / 3) + 1]);
                stack.push(temp);
            }

            String returned = "";
            while (!stack.empty()) {
                returned = stack.pop() + returned;
            }

            return returned;
        }else{
            String value = String.format("%.12f", input);
            for (int i = value.length()-1; i >0 ; i--){
                if(value.charAt(i) == '0'){
                    value = value.substring(0,i);
                }else {
                    break;
                }
            }

            Stack stack = new Stack();
            for (int i = 0; i < value.length() - 2 ; i++) {
                stack.add(value.charAt(value.length()-i-1));
            }

            int zeroCounter = 0;
            char tableOfPrefixes[] = {'m', 'u', 'n', 'p'};

            while (true) {
                Object a = stack.peek();
                Object b = '0';
                if (a == b) {
                    stack.pop();
                    zeroCounter++;
                } else {
                    break;
                }
            }
            Stack stackReversed = new Stack();
            while (!stack.empty()) {
                stackReversed.push(stack.pop());
            }
            stack = stackReversed;

            if (zeroCounter % 3 == 0) {
                if(stack.size() == 1){
                    stack.push("0");
                }
                stack.push("0");
                stack.push(tableOfPrefixes[zeroCounter / 3]);
            } else if (zeroCounter % 3 == 1) {
                if(stack.size() == 1){
                    stack.push("0");
                }
                stack.push(tableOfPrefixes[zeroCounter / 3]);
            } else if (zeroCounter % 3 == 2 && stack.size() == 1) {
                stack.push(tableOfPrefixes[zeroCounter / 3]);
            } else {
                Object temp = stack.peek();
                stack.pop();
                stack.push(tableOfPrefixes[zeroCounter / 3]);
                stack.push(temp);
            }


            String returned = "";
            while (!stack.empty()) {
                returned = stack.pop()+ returned;
            }
            return returned;
        }
    }
    private boolean calculateFilter(double frequency){

        Hashtable<JCheckBox,Double> resistorMultiplerValue = new Hashtable(5);
        for (int i=0; i<resistorMultiplers.size(); i++){
            resistorMultiplerValue.put(resistorMultiplers.get(i), Math.pow(10,i+1));
        }

        Hashtable<JCheckBox,Double> capacitorMultiplerValue = new Hashtable(10);
        for (int i=0; i< capacitorMultiplers.size(); i++){
            capacitorMultiplerValue.put(capacitorMultiplers.get(i), 0.0000000000001 * Math.pow(10,i+1));
        }

        int resistorSeries[] = getSeries(comboBox1.getSelectedItem().toString());
        int capacitorSeries[] = getSeries(comboBox2.getSelectedItem().toString());

        for (int i =0; i<resistorMultiplers.size(); i++){
            if (resistorMultiplers.get(i).isSelected()){
                System.out.println(resistorMultiplerValue.get(resistorMultiplers.get(i)));
                for (int j =0; j<capacitorMultiplers.size(); j++){
                    if (capacitorMultiplers.get(j).isSelected()){
                        for (int k=0; k< resistorSeries.length; k++){
                            for (int m=0; m <capacitorSeries.length; m++){
                                Resistor resistor = new Resistor(resistorMultiplerValue.get(resistorMultiplers.get(i)) * resistorSeries[k]);
                                Capacitor capacitor = new Capacitor(capacitorMultiplerValue.get(capacitorMultiplers.get(j)) * capacitorSeries[m]);
                                Filter filter;
                                if (filterType.getSelection().equals(highPassRCRadioButton.getModel())){
                                    HRC temp = new HRC(resistor, capacitor);
                                    filter = temp;
                                }else if (filterType.getSelection().equals(lowPassRCRadioButton.getModel())){
                                    LRC temp = new LRC(resistor, capacitor);
                                    filter = temp;
                                }else if (filterType.getSelection().equals(highPassRLRadioButton.getModel())){
                                    HRL temp = new HRL(resistor, capacitor);
                                    filter = temp;
                                }else{
                                    LRL temp = new LRL(resistor, capacitor);
                                    filter = temp;
                                }
                                double error = Math.abs(filter.getValue() - frequency);
                                if (error < frequency*0.05){
                                    calculatedLabel.setText("Resistor " + convertToStandardNotation(resistor.getValue())+  "Ohm, capacitor " + convertToStandardNotation(capacitor.getValue())  + "F, cut-off frequency " + ((int) filter.getValue()) + "Hz");
                                    try{
                                        BufferedImage image = ImageIO.read(new File(filter.getImageUrl()));
                                        pictureLabel.setIcon(new ImageIcon(image));
                                    }
                                    catch (IOException ex){
                                        JOptionPane.showMessageDialog(new JFrame("Error"),"Error - cannot find pictures");
                                    }
                                    return true;
                                }
                            }
                        }

                    }
                }
            }
        }
        calculatedLabel.setText("Cannot create a filter");
        return false;
    }
    //@Override
    private void createUIComponents() {

    }
}

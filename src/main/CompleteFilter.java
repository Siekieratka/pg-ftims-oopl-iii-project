package main;

/**
 * Created by rafal on 18.01.2016.
 */
public interface CompleteFilter {
    double getValue();
    double getElementsValues();
    String getImageUrl();
}
